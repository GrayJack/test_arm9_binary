#!/bin/bash

TARGET=thumbv5te-none-eabi
#
# echo "Compiling program \"$C9_PROG_TYPE\"..."

RELMODE="$1"
if [ "$1" = "release" ]; then
    RELMODE="release"
    RELMODE_FLAG="--release"
else
    RELMODE="debug"
    RELMODE_FLAG=""
fi

export CROSS_COMPILE=arm-none-eabi-
export RUST_TARGET_PATH="$(pwd)"
export CARGO_TARGET_DIR="$RUST_TARGET_PATH/target"

if [ "$1" = "doc" ]; then
    rustup run nightly cargo xdoc $2 || exit -1
else
    rustup run nightly cargo xbuild $RELMODE_FLAG || exit -1
fi

TARGET_DIR="./target/${TARGET}/${RELMODE}"
TARGET_ELF="${TARGET_DIR}/test_arm"
TARGET_BIN="${TARGET_DIR}/test_arm.bin"
TARGET_FIRM="${TARGET_DIR}/test_arm.firm"

arm-none-eabi-objcopy -O binary -I elf32-littlearm "$TARGET_ELF" "$TARGET_BIN" || exit -1
firmtool build "$TARGET_FIRM" -i -n 0x23F00000 -e 0 -D "$TARGET_BIN" -A 0x23F00000 -C NDMA || exit -1
