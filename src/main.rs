#![no_std]
#![no_main]
#![feature(alloc_error_handler)]

use arm9_intrinsics::power;
use arm9_intrinsics::exception::*;

mod mem;

#[no_mangle]
extern fn main() {
    unsafe {
        // Don't allow jumping to anywhere in ITCM
        for i in 0..0x2000 {
            *((4*i) as *mut u32) = 0xf7f0a000;
        }
    }

    power::power_off();
}

#[global_allocator]
pub(crate) static ALLOCATOR: mem::Allocator = mem::Allocator::new();

#[alloc_error_handler]
extern fn alloc_error(_: core::alloc::Layout) -> ! {
    panic!("Out of memory!");
}

#[panic_handler]
pub fn panic_handler(info: &core::panic::PanicInfo) -> ! {
    power::reboot()
}

#[no_mangle]
pub extern fn abort() -> ! {
    loop {
        unsafe { arm9_intrinsics::exception::wait_for_interrupt() };
    }
}
